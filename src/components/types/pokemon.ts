
type PokemonTypes ={
    slot:number,
    type:{
        name:string,
        url:string
    }
}
type PokemonData ={
    id: number,
    order: number,
    types: string[]
}
export type PokemonType = {
   name: string,
   url: string,
   data: PokemonData,
   types: PokemonTypes

}